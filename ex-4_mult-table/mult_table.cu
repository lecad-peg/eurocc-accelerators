#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime.h>

__global__ void multTable(int *out, int *a, int *b, int m, int n) {

    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    
    if (i < m || j < n)
    {
        out[i*n+j] = a[i] * b[j];
    }
}

int main(){
    int m = 10;
    int n = 10;

    int *a, *b, *out;
    int *d_a, *d_b, *d_out; 

    // Allocate host memory
    a   = (int*)malloc(sizeof(int) * m);
    b   = (int*)malloc(sizeof(int) * n);
    out = (int*)malloc(sizeof(int) * m * n);

    // Initialize host arrays
    for(int i = 0; i < m; i++) {
        a[i] = i+1;
    }
    for(int i = 0; i < n; i++) {
        b[i] = i+1;
    }

    // Allocate device memory
    cudaMalloc((int**)&d_a, sizeof(int) * m);
    cudaMalloc((int**)&d_b, sizeof(int) * n);
    cudaMalloc((int**)&d_out, sizeof(int) * m * n);

    // Transfer data from host to device memory
    cudaMemcpy(d_a, a, sizeof(int) * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(int) * n, cudaMemcpyHostToDevice);

    // Executing kernel
    dim3 dimBlock(5, 2);
    dim3 dimGrid(2, 5);

    multTable<<<dimBlock, dimGrid>>>(d_out, d_a, d_b, m, n);
        
    // Transfer data back to host memory
    cudaMemcpy(out, d_out, sizeof(int) * m * n, cudaMemcpyDeviceToHost);

    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {
            printf("%d * %d = %d\n", a[i], b[j], out[i*n+j]);
        }
        printf("######\n");
    }

    // Deallocate device memory
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_out);

    // Deallocate host memory
    free(a); 
    free(b); 
    free(out);
}
