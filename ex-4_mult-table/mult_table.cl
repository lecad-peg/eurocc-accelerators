__kernel void multTable(__global const int *a,
		        __global const int *b,		
			__global int *out,				
		        int m,
		        int n)						
{														
    int i = get_global_id(0);
    int j = get_global_id(1);

    if (i < m || j < n)
    {
        out[i*n+j] = a[i] * b[j];
    }
}
