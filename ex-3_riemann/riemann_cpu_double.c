#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <string.h>
#include <stdbool.h>

#define N 1000000000

double riemann(int n)
{
  double sum = 0;
  for(int i = 0; i < n; ++i)
  {
    double x = (double) i / (double) n;
    double fx = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
    sum += fx;
  }
  sum *= (1.0 / sqrt(2.0 * M_PI)) / (double) n;
  return sum;
}

int main(int argc, char** argv){

  clock_t t1; 
  t1 = clock();

  double sum = riemann(N);

  t1 = clock() - t1;

  double time_taken1 = ((double)t1)/CLOCKS_PER_SEC; // in seconds

  printf("Riemann sum CPU (double precision) for N = %d     : %.17g \n", N, sum);
  printf("Total time (measured by CPU)                              : %f s\n", time_taken1);
} 
