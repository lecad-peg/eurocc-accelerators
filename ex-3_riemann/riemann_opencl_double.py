from __future__ import absolute_import, print_function
import numpy as np
import pyopencl as cl

import time

def iDivUp(a, b):
    return a // b + 1

N = 1000000000

def riemannOpenCL(n):

    a = np.empty([n])
    a = a.astype(np.float64)

    queue = cl.CommandQueue(ctx)

    mf = cl.mem_flags
    a_d = cl.Buffer(ctx, mf.WRITE_ONLY, a.nbytes)

    prg = cl.Program(ctx, """
    __kernel void medianTrapezoid(__global double *a, int n) {
    
        int idx = get_global_id(0);
        double x = (double)idx / (double)n;
 
        if(idx < n)
           a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
    }
    """).build()

    local_item_size = 1024
    n_blocks = iDivUp(n, local_item_size)
    global_item_size = n_blocks * local_item_size
    print("OpenCL kernel 'medianTrapezoid' launch with %i blocks of %i threads\n" % (n_blocks, local_item_size))
    prg.medianTrapezoid(queue, (global_item_size, 1, 1), (local_item_size, 1, 1), a_d, np.int32(n))

    cl.enqueue_copy(queue, a, a_d)

    Sum = np.sum(a) / np.sqrt(2 * np.pi) / np.float64(n)

    return Sum

platform = cl.get_platforms()[0]
device = platform.get_devices()[0]
ctx = cl.Context([device])

dev_name = device.name
total_memory = device.global_mem_size / 1024.0 / 1024.0 / 1024.0
threads_per_block = device.max_work_group_size
sm_count = device.max_compute_units

print("Found GPU '%s' with %.3f GB of global memory, max %i threads per block, and %i multiprocessors\n" % 
       (dev_name, total_memory, threads_per_block, sm_count))

start = time.time()

Sum = riemannOpenCL(N)

end = time.time()

time_taken = end - start # in seconds

print("Riemann sum pyOpenCL (double precision) for N = %i  : %.17f" % (N, Sum));
print("Total time (measured by CPU)                                : %f s" % time_taken);
