#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
 
#define N 1000000000
 
/* CUDA error wraper */
static void CUDA_ERROR( cudaError_t err) 
{
    if (err != cudaSuccess) {
        printf("CUDA ERROR: %s, exiting\n", cudaGetErrorString(err));
        exit(-1);
    }
}

__global__ void medianTrapezoid(double *a, int n)
{
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  double x = (double)idx / (double)n;
 
  if(idx < n)
    a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
}
 
double riemannCUDA(int n)
{
  ///size of the arrays in bytes
  size_t size = n * sizeof(double);
 
  // allocate array on host and device
  double* a_h = (double *)malloc(size);
  double* a_d; cudaMalloc((double **) &a_d, size);
 
  // do calculation on device
  int block_size = 1024;
  int n_blocks = n/block_size + (n % block_size == 0 ? 0:1);
  printf("CUDA kernel 'medianTrapezoid' launch with %d blocks of %d threads\n", n_blocks, block_size);
  medianTrapezoid <<< n_blocks, block_size >>> (a_d, n);
  
  // copy results from device to host
  cudaMemcpy(a_h, a_d, sizeof(double)*n, cudaMemcpyDeviceToHost);
 
  // add up results
  double sum = 0;
  for (int i=0; i < n; i++) sum += a_h[i];
  sum *= (1.0 / sqrt(2.0 * M_PI)) / (double)n;
  
  // clean up
  free(a_h); cudaFree(a_d);
  
  return sum;
}
 
int main(int argc, char** argv){
 
  /*get info on our GPU, defaulting to first one*/
  cudaDeviceProp prop;
  CUDA_ERROR(cudaGetDeviceProperties(&prop,0));
  printf("Found GPU '%s' with %g GB of global memory, max %d threads per block, and %d multiprocessors\n", 
         prop.name, prop.totalGlobalMem/(1024.0*1024.0*1024.0),
         prop.maxThreadsPerBlock,prop.multiProcessorCount);
 
  /*init CUDA*/
  CUDA_ERROR(cudaSetDevice(0));
 
  clock_t t1; 
  t1 = clock();
 
  double sum = riemannCUDA(N);
 
  t1 = clock() - t1;
 
  double time_taken1 = ((double)t1)/CLOCKS_PER_SEC; // in seconds
 
  printf("Riemann sum CUDA (double precision) for N = %d    : %.17g \n", N, sum);
  printf("Total time (measured by CPU)                              : %f s\n", time_taken1);
} 
