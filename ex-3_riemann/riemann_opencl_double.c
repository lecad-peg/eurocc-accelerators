#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <CL/cl.h>

#define MAX_SOURCE_SIZE (0x100000)

#define N 1000000000

double riemannCL(int n)
{
    //Allocate memory to host variable
    double *a = (double*)malloc(sizeof(double)*n);
    
    // Load the kernel source code into the array source_str
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("riemann.cl", "r");
    if (!fp) {
        fprintf(stderr, "Failed to load kernel.\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread( source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose( fp );

    // Get platform and device information
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;   
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
    ret = clGetDeviceIDs( platform_id, CL_DEVICE_TYPE_ALL, 1, 
            &device_id, &ret_num_devices);

    // Create an OpenCL context
    cl_context context = clCreateContext( NULL, 1, &device_id, NULL, NULL, &ret);

    // Create a command queue
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

    // Create memory buffers on the device for each vector 
    cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_WRITE, 
            n * sizeof(double), NULL, &ret);

    // Create a program from the kernel source
    cl_program program = clCreateProgramWithSource(context, 1, 
            (const char **)&source_str, (const size_t *)&source_size, &ret);

    // Build the program
    ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

    clock_t t2; 
    t2 = clock(); 

    // Create the OpenCL kernel
    cl_kernel kernel = clCreateKernel(program, "medianTrapezoid", &ret);

    // Set the arguments of the kernel
    ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);    
    ret = clSetKernelArg(kernel, 1, sizeof(cl_int), (void *)&n);
    
    // Execute the OpenCL kernel
    size_t local_item_size = 1024;
    int n_blocks = n/local_item_size + (n % local_item_size == 0 ? 0:1);
    size_t global_item_size = n_blocks * local_item_size;
    printf("OpenCL kernel 'medianTrapezoid' launch with %d blocks of %lu threads\n\n", n_blocks, local_item_size);

    ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, 
            &global_item_size, &local_item_size, 0, NULL, NULL);

    t2 = clock() - t2;

    double time_taken2 = ((double)t2)/CLOCKS_PER_SEC; // in seconds

    clock_t t3; 
    t3 = clock();

    ret = clEnqueueReadBuffer(command_queue, a_mem_obj, CL_TRUE, 0, 
            n * sizeof(double), a, 0, NULL, NULL);

    t3 = clock() - t3;

    double time_taken3 = ((double)t3)/CLOCKS_PER_SEC; // in seconds

    clock_t t4; 
    t4 = clock(); 

    // add up results
    double sum = 0;
    for (int i=0; i < n; i++) sum += a[i];
    sum *= (1.0 / sqrt(2.0 * M_PI)) / (double)n;

    t4 = clock() - t4;

    double time_taken4 = ((double)t4)/CLOCKS_PER_SEC; // in seconds

    // Clean up
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(a_mem_obj); 
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);
    free(a);

    printf("OpenCL and CPU code diagnostics:\n");
    printf("OpenCL kernel execution time (measured by CPU):        %f ms\n", time_taken2 * 1000);
    printf("Device to host memory transfer time (measured by CPU): %f s\n", time_taken3);
    printf("CPU execution time for adding sum (measured by CPU):   %f s\n\n", time_taken4);
  
    return sum;
}

int main(int argc, char** argv){

  clock_t t1; 
  t1 = clock(); 

  double sum = riemannCL(N);

  t1 = clock() - t1;

  double time_taken1 = ((double)t1)/CLOCKS_PER_SEC; // in seconds

  printf("Riemann sum OpenCL (double precision) for N = %d    : %.17g \n", N, sum);
  printf("Total time (measured by CPU)                                : %f s\n", time_taken1);
}
