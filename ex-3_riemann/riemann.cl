__kernel void medianTrapezoid(__global double *a, int n) {
    
    int idx = get_global_id(0);
    double x = (double)idx / (double)n;
 
    if(idx < n)
       a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)n) * (x + 1 / (double)n) / 2.0)) / 2.0;
}
