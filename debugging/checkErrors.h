#include <CL/cl.h>

#include <stdio.h>
#include <stdlib.h>

void checkErrors(cl_int status, char *label, int line)
{
  switch (status)
  {
      case CL_SUCCESS:
        return;
      case CL_BUILD_PROGRAM_FAILURE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_BUILD_PROGRAM_FAILURE\n", label, line);
        break;
      case CL_COMPILER_NOT_AVAILABLE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_COMPILER_NOT_AVAILABLE\n", label, line);
        break;
      case CL_DEVICE_NOT_AVAILABLE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_DEVICE_NOT_AVAILABLE\n", label, line);
        break;
      case CL_DEVICE_NOT_FOUND:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_DEVICE_NOT_FOUND\n", label, line);
        break;
      case CL_IMAGE_FORMAT_MISMATCH:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_IMAGE_FORMAT_MISMATCH\n", label, line);
        break;
      case CL_IMAGE_FORMAT_NOT_SUPPORTED:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_IMAGE_FORMAT_NOT_SUPPORTED\n", label, line);
        break;
      case CL_INVALID_ARG_INDEX:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_ARG_INDEX\n", label, line);
        break;
      case CL_INVALID_ARG_SIZE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_ARG_SIZE\n", label, line);
        break;
      case CL_INVALID_ARG_VALUE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_ARG_VALUE\n", label, line);
        break;
      case CL_INVALID_BINARY:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_BINARY\n", label, line);
        break;
      case CL_INVALID_BUFFER_SIZE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_BUFFER_SIZE\n", label, line);
        break;
      case CL_INVALID_BUILD_OPTIONS:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_BUILD_OPTIONS\n", label, line);
        break;
      case CL_INVALID_COMMAND_QUEUE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_COMMAND_QUEUE\n", label, line);
        break;
      case CL_INVALID_CONTEXT:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_CONTEXT\n", label, line);
        break;
      case CL_INVALID_DEVICE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_DEVICE\n", label, line);
        break;
      case CL_INVALID_DEVICE_TYPE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_DEVICE_TYPE\n", label, line);
        break;
      case CL_INVALID_EVENT:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_EVENT\n", label, line);
        break;
      case CL_INVALID_EVENT_WAIT_LIST:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_EVENT_WAIT_LIST\n", label, line);
        break;
      case CL_INVALID_GL_OBJECT:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_GL_OBJECT\n", label, line);
        break;
      case CL_INVALID_GLOBAL_OFFSET:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_GLOBAL_OFFSET\n", label, line);
        break;
      case CL_INVALID_HOST_PTR:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_HOST_PTR\n", label, line);
        break;
      case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_IMAGE_FORMAT_DESCRIPTOR\n", label, line);
        break;
      case CL_INVALID_IMAGE_SIZE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_IMAGE_SIZE\n", label, line);
        break;
      case CL_INVALID_KERNEL_NAME:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_KERNEL_NAME\n", label, line);
        break;
      case CL_INVALID_KERNEL:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_KERNEL\n", label, line);
        break;
      case CL_INVALID_KERNEL_ARGS:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_KERNEL_ARGS\n", label, line);
        break;
      case CL_INVALID_KERNEL_DEFINITION:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_KERNEL_DEFINITION\n", label, line);
        break;
      case CL_INVALID_MEM_OBJECT:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_MEM_OBJECT\n", label, line);
        break;
      case CL_INVALID_OPERATION:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_OPERATION\n", label, line);
        break;
      case CL_INVALID_PLATFORM:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_PLATFORM\n", label, line);
        break;
      case CL_INVALID_PROGRAM:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_PROGRAM\n", label, line);
        break;
      case CL_INVALID_PROGRAM_EXECUTABLE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_PROGRAM_EXECUTABLE\n", label, line);
        break;
      case CL_INVALID_QUEUE_PROPERTIES:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_QUEUE_PROPERTIES\n", label, line);
        break;
      case CL_INVALID_SAMPLER:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_SAMPLER\n", label, line);
        break;
      case CL_INVALID_VALUE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_VALUE\n", label, line);
        break;
      case CL_INVALID_WORK_DIMENSION:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_WORK_DIMENSION\n", label, line);
        break;
      case CL_INVALID_WORK_GROUP_SIZE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_WORK_GROUP_SIZE\n", label, line);
        break;
      case CL_INVALID_WORK_ITEM_SIZE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_INVALID_WORK_ITEM_SIZE\n", label, line);
        break;
      case CL_MAP_FAILURE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_MAP_FAILURE\n", label, line);
        break;
      case CL_MEM_OBJECT_ALLOCATION_FAILURE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_MEM_OBJECT_ALLOCATION_FAILURE\n", label, line);
        break;
      case CL_MEM_COPY_OVERLAP:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_MEM_COPY_OVERLAP\n", label, line);
        break;
      case CL_OUT_OF_HOST_MEMORY:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_OUT_OF_HOST_MEMORY\n", label, line);
        break;
      case CL_OUT_OF_RESOURCES:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_OUT_OF_RESOURCES\n", label, line);
        break;
      case CL_PROFILING_INFO_NOT_AVAILABLE:
        fprintf(stderr, "OpenCL error (at %s, line %d): CL_PROFILING_INFO_NOT_AVAILABLE\n", label, line);
        break;
  }
  exit(status);
}
