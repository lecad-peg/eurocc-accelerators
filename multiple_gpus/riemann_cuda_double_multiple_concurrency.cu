#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
 
#define N 2000000000

__global__ void medianTrapezoid(double *a, int n, int dev)
{
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  double x = (double)(idx + n * dev) / (double)(2 * n);
 
  if(idx < n)
    a[idx] = (exp(-x * x / 2.0) + exp(-(x + 1 / (double)(2 * n)) * (x + 1 / (double)(2 * n)) / 2.0)) / 2.0;
}
 
double riemannCUDA(int n)
{
  //size of the arrays in bytes
  size_t size = n/2 * sizeof(double);

  
  int block_size = 1024;
  int n_blocks = n/2/block_size + (n/2 % block_size == 0 ? 0:1);

  // memory allocation
  // switch to GPU 0
  cudaSetDevice(0);
  double* a_d1; cudaMalloc((double **) &a_d1, size);
  double* a_h1;  cudaMallocHost((void**)&a_h1, size);
  //double* a_h1 = (double *)malloc(size);

  // switch to GPU 1
  cudaSetDevice(1);
  double* a_d2; cudaMalloc((double **) &a_d2, size);
  double* a_h2;  cudaMallocHost((void**)&a_h2, size);
  //double* a_h2 = (double *)malloc(size);

  // do calculations on GPUs and transfer data back to host
  // switch to GPU 0
  int dev = 0;
  cudaSetDevice(0);
  printf("CUDA kernel launch with %d blocks of %d threads\n", n_blocks, block_size);
  medianTrapezoid <<< n_blocks, block_size>>> (a_d1, n/2, dev);

  cudaMemcpyAsync(a_h1, a_d1, sizeof(double)*n/2, cudaMemcpyDeviceToHost);
  //cudaMemcpy(a_h1, a_d1, sizeof(double)*n/2, cudaMemcpyDeviceToHost);

  // switch to GPU 1
  dev = 1;
  cudaSetDevice(1);
  printf("CUDA kernel launch with %d blocks of %d threads\n", n_blocks, block_size);
  medianTrapezoid <<< n_blocks, block_size >>> (a_d2, n/2, dev);
  cudaMemcpyAsync(a_h2, a_d2, sizeof(double)*n/2, cudaMemcpyDeviceToHost);
  //cudaMemcpy(a_h2, a_d2, sizeof(double)*n/2, cudaMemcpyDeviceToHost);

  // synchronize GPUs
  cudaDeviceSynchronize();

  // add up results
  double sum1 = 0;
  double sum2 = 0;
  for (int i=0; i < n/2; i++){
      sum1 += a_h1[i];
      sum2 += a_h2[i];
  }
  
  double sum;
  sum = sum1 + sum2;
  sum *= (1.0 / sqrt(2.0 * M_PI)) / (double)n;

  // clean up
  // switch to GPU 0
  cudaSetDevice(0);
  cudaFree(a_h1); cudaFree(a_d1);

  // switch to GPU 1
  cudaSetDevice(1);
  cudaFree(a_h2); cudaFree(a_d2);
  
  return sum;
}
 
int main(int argc, char** argv){
 
  clock_t t1; 
  t1 = clock();
 
  double sum = riemannCUDA(N);
 
  t1 = clock() - t1;
 
  double time_taken1 = ((double)t1)/CLOCKS_PER_SEC; // in seconds
 
  printf("Riemann sum CUDA (double precision) for N = %d    : %.17g \n", N, sum);
  printf("Total time (measured by CPU)                              : %f s\n", time_taken1);
} 
