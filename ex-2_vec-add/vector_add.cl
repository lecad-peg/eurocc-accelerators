__kernel void vector_add(__global double *a, __global double *b, __global double *out, int n) {
    
    // Get the index of the current element
    int i = get_global_id(0);

    // Do the operation
    if(i < n)
      out[i] = a[i] + b[i];
}
