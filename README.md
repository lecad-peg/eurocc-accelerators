# eurocc-accelerators #

Code examples for the EuroCC/SLING Training Course "Introduction to parallel programming: accelerators".

## Compiling, running and profiling CUDA and OpenCL codes

### Setting the environment

On `viz.hpc.fs.uni-lj.si` CUDA module for compiling CUDA and OpenCL codes:

    $ module load CUDA/10.1.243-GCC-8.3.0

or TAU module with Java Runtime Environment enabled for compiling and/or profiling CUDA and OpenCL codes is needed:

    $ module purge
    $ module load tau/2.29.1-CUDA
    $ module load jre

Profiling can be done only on the GPU node, an interactive session on it can be started with:

    $ env --unset=LD_PRELOAD TMOUT=600 srun --time=1:0:0 --partition=gpu --x11 --pty bash -i

### Compiling and running

**CUDA codes** can be compiled and run, e.g., with:

    $ nvcc -o hello_cuda hello.cu
    $ ./hello_cuda

**OpenCL codes** can be compiled and run, e.g., with:

    $ gcc -o hello_CL hello_CL.c -lOpenCL
    $ ./hello_CL

## Running pyCUDA and pyOpenCL scripts

On `viz.hpc.fs.uni-lj.si` a Python virtual environment must be created for running pyCUDA and pyOpenCL scripts. Follow the instructions below to create, set, load and use the virtual environment.

### Setting virtual environment

For every session in a terminal these modules must be loaded:

    $ module load Python/3.7.4-GCCcore-8.3.0
    $ module load CUDA/10.1.243-GCC-8.3.0

Create virtual environment (venv), e.g., in home directory - just once:

    $ virtualenv pygpu

Activate venv (for every session in a terminal):

    $ source pygpu/bin/activate
    (pygpu) [campus03@viz ~]$

Check Python version:

    (pygpu) [campus03@viz ~]$ python -V
    Python 3.7.2

Install **pycuda** and **pyopencl** (in venv) - just once:

    $ pip install pycuda pyopencl

Check installed Python libraries:

    $ pip list

### Running scripts

Python scripts are run, e.g., with:

    (pygpu) [campus03@viz ~]$ python riemann_cuda_double.py

Deactivate venv (after finishing work):

    (pygpu) [campus03@viz ~]$ deactivate
    [campus03@viz ~]$
